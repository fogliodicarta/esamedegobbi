-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: AziendaEsame
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authEmp`
--

DROP TABLE IF EXISTS `authEmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authEmp` (
  `employeeId` int(11) NOT NULL,
  `authorizationId` int(11) NOT NULL,
  PRIMARY KEY (`employeeId`,`authorizationId`),
  KEY `fkauth` (`authorizationId`),
  CONSTRAINT `fkauth` FOREIGN KEY (`authorizationId`) REFERENCES `authorization` (`idAuthorization`),
  CONSTRAINT `fkemployee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`idEmployee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authEmp`
--

LOCK TABLES `authEmp` WRITE;
/*!40000 ALTER TABLE `authEmp` DISABLE KEYS */;
INSERT INTO `authEmp` VALUES (1,1),(1,8),(2,3),(2,6),(2,7),(3,5),(4,5),(4,8),(5,6),(6,6),(7,8);
/*!40000 ALTER TABLE `authEmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorization`
--

DROP TABLE IF EXISTS `authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization` (
  `idAuthorization` int(11) NOT NULL,
  `livello` int(11) DEFAULT NULL,
  `tipo` enum('finance','it','hr','legale','erp') DEFAULT NULL,
  PRIMARY KEY (`idAuthorization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization`
--

LOCK TABLES `authorization` WRITE;
/*!40000 ALTER TABLE `authorization` DISABLE KEYS */;
INSERT INTO `authorization` VALUES (1,1,'finance'),(2,2,'finance'),(3,3,'finance'),(4,1,'it'),(5,2,'it'),(6,1,'hr'),(7,1,'legale'),(8,1,'erp');
/*!40000 ALTER TABLE `authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `idEmployee` int(11) NOT NULL,
  `codice` char(5) DEFAULT NULL,
  `cognome` varchar(25) DEFAULT NULL,
  `nome` varchar(25) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `password` char(8) DEFAULT NULL,
  PRIMARY KEY (`idEmployee`),
  UNIQUE KEY `codice` (`codice`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'00001','Giallo','Giovanni','gg',''),(2,'0000A','Rossi','Mario','rm',''),(3,'00002','Bianchi','Enrico','be',''),(4,'0000B','Verdi','Arturo','va',''),(5,'00003','Verdi','Giuseppe','vg',''),(6,'0000C','Verdi','Stefano','vs',''),(7,'00004','Grigio','Luca','gl','');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-02 17:24:56
