package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;

public class Main extends Application {
    public static Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("app.fxml"));
        stage=primaryStage;
        primaryStage.setTitle("Authorization Manager");
        primaryStage.setScene(new Scene(root, 700, 550));
        primaryStage.show();
        stage.getIcons().add(
                new Image(
                        Main.class.getResourceAsStream( "zucc.png" )));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
