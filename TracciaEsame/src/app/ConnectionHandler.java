package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;

public class ConnectionHandler {
    private static URL url;
    private String fmat;

    public ConnectionHandler() {
        fmat = "http://%s:%s/AdapterFramework/ChannelAdminServlet?&party=*&service=*&channel=%s&action=%s";
    }

    public static String invia() throws MalformedURLException, ProtocolException, IOException, Exception {
        String temp;
        temp = "http://httpbin.org/post";
        url = new URL(temp);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("x-auth-token", "kfksj48sdfj4jd9d");
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode("matteopassword".getBytes()));
        con.setRequestProperty("Authorization", basicAuth);
        con.setDoOutput(true);
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = "{dipendente:'00001,Giovanni Giallo',auths:['finance','it']}".getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        String xmls = content.toString();
        return xmls;
    }

}
