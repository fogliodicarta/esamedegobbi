package app;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.ArrayList;

public class Controller {

    public FileChooser fileChooser = new FileChooser();
    @FXML
    public Button scegli;
    @FXML
    public ListView lista;
    public String path;

    public void scegliFile(){
        try {
            fileChooser.setTitle("Open Resource File");
            File scelto = fileChooser.showOpenDialog(Main.stage);
            scegli.setText(scelto.toString());
            path = scelto.toString();
            lista.getItems().clear();
            ArrayList<String> letti= Lettore.leggiFile(path);
            lista.getItems().addAll(letti); //in questo modo non serve il ciclo

        } catch (Exception e) {
        }

    }

    public void richiesta() throws Exception {
        System.out.println(ConnectionHandler.invia());
    }
}
