package app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


public class Lettore {
    public static ArrayList<String> leggiFile(String percorso) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(percorso));;
        String linea;
        ArrayList<String>letto=new ArrayList<String>();
        do {
            linea=reader.readLine();
            letto.add(linea);
        }while (linea!=null);
        letto.remove(letto.size()-1);
        return letto;
    }
}
